set more off
clear all
cd C:\Users\m.callaghan\Documents\GitHub\statsII_0\assignment\stata

cap log close
log using q1_1, text replace
*@s
capture confirm file data/kerg.csv
if _rc==601 {
	copy https://www.bundeswahlleiter.de/de/bundestagswahlen/BTW_BUND_13/veroeffentlichungen/ergebnisse/kerg.csv ///
		data/kerg.csv, replace
}

import delimited using data\kerg.csv, ///
	delimiters(";") rowrange(3) varnames(3)
*@e
cap log close

log using q1_2, text replace
*@s
local old nr gebiet gehört wahlberechtigte wähler cdu v22 spd v26 ///
fdp v39 dielinke v34 grüne v38 csu v42 pirate v46 afd v106
	
local new no district landid eligible voters cdu1 cdu2 spd1 spd2 ///
	fdp1 fdp2 dielinke1 dielinke2 greens1 greens2 csu1 csu2 pirates1 ///
	pirates2 afd1 afd2

rename (`old') (`new')
*@e
cap log close


log using q1_3, text replace
*@s
keep `new'
*@e
cap log close

log using q1_4, text replace
*@s
drop in 1/2
drop if landid=="99" | district=="Bundesgebiet"
*@e
cap log close

log using q1_5, text replace
*@s
destring landid-afd2, replace
*@e
cap log close

log using q1_6, text replace
*@s
cap gen land = ""
local lands Schleswig-Holstein Hamburg Lower-Saxony Bremen ///
	North-Rhine-Westphalia Hesse Rhineland-Palatinate ///
	Baden-Württemberg Bavaria Saarland Berlin Brandenburg ///
	Mecklenburg-Vorpommern Saxony Saxony-Anhalt Thuringia

local N : word count `lands'
forvalues i = 1/`N' {
	local lname : word `i' of `lands'
	di "`lname'"
	replace land = "`lname'" if landid == `i'
}
*@e
cap log close



log using q1_7, text replace
*@s
cap gen turnout = (voters/eligible) * 100
*@e
cap log close

log using q1_8, text replace
*@s
replace cdu1 = csu1 if landid == 9
replace cdu2 = csu2 if landid == 9
rename cdu1 cducsu1
rename cdu2 cducsu2
drop csu*
*@e
cap log close

log using q1_9, text replace
*@s
foreach var of varlist cducsu1 cducsu2 spd1 spd2 fdp1 fdp2 dielinke1 ///
	dielinke2 greens1 greens2 pirates1 pirates2 afd1 afd2 {
	replace `var' = (`var'/voters) * 100
}
*@e
cap log close

log using q1_10, text replace
*@s
save data/results, replace
clear
*@e
cap log close



********************************************************
** 
log using q2_1, text replace
*@s
clear
capture confirm file data/struktur.csv
if _rc==601 {
	copy https://www.bundeswahlleiter.de/de/bundestagswahlen/BTW_BUND_13/strukturdaten/StruktBtwkr2013.csv ///
		data/struktur.csv, replace
}

import delimited using data\struktur.csv, delimiters(";") rowrange(2) varnames(2)
*@e
cap log close

log using q2_2, text replace
*@s
local old wahlkreisnr flächeam31122011km ///
	bevölkerungam30092012insgesamtin bevölkerungsdichteam31122011einw ///
	v13 v14 v15 v16 v17 v22 gewerbesteuereinnahmen2011euroje ///
	arbeitslosenquoteendedezember201 empfängerinnenvonleistungennachs
	
local new no area population popdensity pop18_35 pop25_35 pop35_60 pop60_75 ///
	pop75_ abitur localbusinesstax unemployment socialbenefits
	
rename (`old') (`new')
*@e
cap log close


log using q2_3, text replace
*@s
keep `new'
*@e
cap log close

log using q2_4, text replace
*@s
drop in 1
keep if no < 900
*@e
cap log close

log using q2_5, text replace
*@s
destring,dpcomma ignore(" ") replace
*@e
cap log close

log using q2_6, text replace
*@s
save data/localstats, replace
clear
use data/results.dta
merge 1:1 no using data/localstats
drop _merge
save data/electionData, replace
*@e
cap log close

log using q3_1, text replace
*@s
eststo A: quietly reg turnout unemployment
esttab A using unemployment_turnout.rtf, replace
*@e
esttab A using unemployment_turnout.tex, replace
cap log close

logs2rtf
