Const ForReading = 1

Dim oShell : Set oShell = CreateObject("WScript.Shell")
oShell.CurrentDirectory = "C:\Users\m.callaghan\Documents\GitHub\statsii_0\assignment\stata"

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFolder = objFSO.GetFolder("C:\Users\m.callaghan\Documents\GitHub\statsii_0\assignment\stata")
strStartText = "*@s"
strEndText = "*@e"

Set colFiles = objFolder.Files
For Each objFile in colFiles
	If instr(objFile.Name,".log") > 0 Then
		strFileName = replace(objFile.Name,".log",".rtf") 
		strFileName2 = objFile.Name 
		Set objFSO = CreateObject("Scripting.FileSystemObject") 
		Set objFile = objFSO.OpenTextFile("C:\Users\m.callaghan\Documents\GitHub\statsii_0\assignment\stata\"  & strFileName2, ForReading) 

		strContents = objFile.ReadAll 

		echo = "TRUE"
		results = "TRUE"
		newText = ""
		write = 0
		lines = Split(strContents,VbCrLf,-1)
		for each l in lines

			If instr(l,strEndText) > 0 Then
				write = 0
			End If


			If write = 1 Then
				fc = Left(l,1)
				If instr(echo,"FALSE") > 0 Then
					If instr(fc,".") = 1 Then
					l = "blank"
					End If
				End If
				If instr(results,"FALSE") > 0 Then
					If NOT fc = "." Then
					l = "blank"
					End If
				End If
				If NOT l = "blank" Then
					newText = newText + l + VbCrLf
				End If
			End if

			If instr(l,strStartText ) > 0 Then
				write = 1
				intArgsStart = InStr(l, "{")
				intArgsEnd = InStr(l, "}")
				if intArgsEnd > intArgsStart Then
					strArgs = Mid(l, intArgsStart + 1, intArgsEnd - intArgsStart - 1) 
				End If
				Args = Split(strArgs,",",-1)
				for each a in Args
					arg = Split(a,"=",-1)
					If instr(a,"echo") > 0 Then
						echo = arg(1)
					End If
					If instr(a,"results") > 0 Then
						results = arg(1)
					End If
				next
			End If

		Next
		strText = newText
		Set newobjFile = objFSO.CreateTextFile("C:\Users\m.callaghan\Documents\GitHub\statsii_0\assignment\stata\" & strFileName) 
		strText = replace(strText,VbCrLf,"\line ") 
		newobjFile.Write "{\rtf1\utf-8\deff0{\fonttbl{\f0 courier;}}{\colortbl\red0\green0\blue0;\red0\green0\blue0;}\cf1\fs16 " & strText & "}" 
		objFile.Close 
	End If 
Next
